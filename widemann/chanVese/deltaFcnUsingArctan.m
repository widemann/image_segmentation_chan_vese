%% y = deltaFcnUsingArctan(x, varargin)
% Smoothed version of the delta function using arctan. 
% y = (1/pi)(epsilon/(epsilon^2 + x^2))
% Input:
%   x, array of doubles of any size. 
%   epsilon = varargin{1}
% Output:
%   y = (1/pi)(epsilon/(epsilon^2 + x.^2));
% Example:
%   x = linspace(-1e-5,1e-5,1e3);
%   y = deltaFcnUsingArctan(x);
%   figure, plot(x,y);
function y = deltaFcnUsingArctan(x, varargin)
epsilon = 1e-6;
if nargin > 1
    epsilon = varargin{1};
end
y = (1/pi)*(epsilon./(epsilon^2 + x.^2));

end

