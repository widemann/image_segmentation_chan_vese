%% y = HeavisideUsingArctan(x, varargin)
% Smoothed version of the Heaviside function.
% y = (1/2)(1 + (2/pi)*atan(x/epsilon))
% Input:
%   x, array of doubles of any size. 
%   epsilon = varargin{1}
% Output:
%   y = (1/2)(1 + (2/pi)*atan(x/epsilon))
% Example:
%   x = linspace(-1e-5,1e-5,1e3);
%   y = HeavisideUsingArctan(x);
%   figure, plot(x,y);
function y = HeavisideUsingArctan(x, varargin)
epsilon = 1e-6;
if nargin > 1
    epsilon = varargin{1};
end
y = .5*(1 + (2/pi)*atan(x/epsilon));

end

