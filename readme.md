This code implements the Chan-Vese algorithm for image segmentation. It was coded by:

**Yue Wu @ ECE Department at Tufts University**

The goal was to see if the algorithm is able to segment amyloids in the brain. Dr. Johnson thinks that these amyloid plaques cause Alzheimer's disease. To run the code, run the file 

**demo.m** 

See the slides in **beta and a platelet activation marker.pptx** for more details. 

The results are that Chan-Vese does a very good job of finding the boundary of the amyloid. They are using the algorithm for their Alzheimer's research. 

